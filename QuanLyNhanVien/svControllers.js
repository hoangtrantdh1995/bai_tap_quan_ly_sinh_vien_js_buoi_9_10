// lấy thông tin từ user 
function getInformationFromForm() {
    var account = document.getElementById("tknv").value.trim();
    var fullName = document.getElementById("name").value.trim();
    var email = document.getElementById("email").value.trim();
    var password = document.getElementById("password").value.trim();
    var workDay = document.getElementById("datepicker").value.trim();
    var baseSalary = document.getElementById("luongCB").value.trim();
    var position = document.getElementById("chucvu").value.trim();
    var workTime = document.getElementById("gioLam").value.trim();
    var totalSalary = calcSalary(position, baseSalary)

    // tạo ofject từ thông tin lấy từ form
    return new staffs(account, fullName, email, password, workDay, baseSalary, position, workTime, totalSalary);
};

// render danh sách xuống
function renderStafftList() {
    var list = getLocalStorage(ST)
    var staffList = list.map(function (item) {
        // map : là nhân đôi giá trị trong array
        return new staffs(
            item.account,
            item.fullName,
            item.email,
            item.password,
            item.workDay,
            item.baseSalary,
            item.position,
            item.workTime,
            item.totalSalary
        );
    });
    // contentHTML là 1 chuổi chứa các thẻ tr sau này sẽ innerHTLM vào thẻ tbody
    var contentHTML = "";

    for (var i = 0; i < staffList.length; i++) {
        var currentStaff = staffList[i];

        var contentTr = `<tr>
    <td>${currentStaff.account}</td>
    <td>${currentStaff.fullName}</td>
    <td>${currentStaff.email}</td>
    <td>${currentStaff.workDay}</td>
    <td>${currentStaff.getPosition()}</td>
    <td>${numberFormat(currentStaff.totalSalary)}</td>
    <td>${currentStaff.getClassification()}</td>
    <td>
    <button onclick ="deleteStaff('${currentStaff.account}')" class ="btn btn-danger">Xóa nhân viên</button>
    <button onclick ="editStaff('${currentStaff.account}')" class ="btn btn-primary" data-toggle="modal"
    data-target="#myModal">Chỉnh Sửa nhân viên</button>
    </td>
    </tr>`;
        // cộng dồn thẻ tr
        contentHTML += contentTr;
    };
    document.getElementById("tableDanhSach").innerHTML = contentHTML;

    document.getElementById("tknv").disabled = false;
};

function showInformationFromForm(infor) {
    document.getElementById("tknv").value = infor.account;
    document.getElementById("tknv").disabled = true;
    document.getElementById("name").value = infor.fullName;
    document.getElementById("email").value = infor.email;
    document.getElementById("password").value = infor.password;
    document.getElementById("datepicker").value = infor.workDay;
    document.getElementById("luongCB").value = infor.baseSalary;
    document.getElementById("chucvu").value = infor.position;
    document.getElementById("gioLam").value = infor.workTime;

};

// reset form
function resetForm() {
    document.getElementById("reset-form-staff").reset();
};

// reset msg

function resetMsg() {
    document.getElementById('tbTKNV').innerText = '';
    document.getElementById('tbChucVu').innerText = '';
    document.getElementById('tbLuongCB').innerText = '';
    document.getElementById('tbNgay').innerText = '';
    document.getElementById('tbMatKhau').innerText = '';
    document.getElementById('tbEmail').innerText = '';
    document.getElementById('tbTen').innerText = '';
    document.getElementById('tbGiolam').innerText = '';
};

