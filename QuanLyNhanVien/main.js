//  Bài toán CRUD ~ Create Read Update Devare
const ST = "staff_list";
// tạo array để lưu danh sách sinh viên
// var staffList = [];

// lấy dữ liệu lên từ localStorage
// var dataJson = localStorage.getItem(ST);

var levels = [
    { id: '1', name: 'Sếp', salary_rate: 3 },
    { id: '2', name: 'Trưởng phòng', salary_rate: 2 },
    { id: '3', name: 'Nhân viên', salary_rate: 1 },
];

var classificationCond = [
    { time: 192, name: 'Xuất Sắc' },
    { time: 176, name: 'Giỏi' },
    { time: 160, name: 'Khá' },
    { time: 0, name: 'Trung Bình' },
];

// if (dataJson) {
//     // truthy - falsy : gần đúng sẽ cho là đúng, gần sai sẽ cho là sai

//     var dataRaw = JSON.parse(dataJson);

//     staffList = dataRaw.map(function (item) {
//         // map : là nhân đôi giá trị trong array
//         return new staffs(
//             item.account,
//             item.fullName,
//             item.email,
//             item.password,
//             item.workDay,
//             item.baseSalary,
//             item.position,
//             item.workTime,
//         );
//     });
//     renderStafftList(staffList);
// };

// lưu xuống local
function initLocalStorage(key) {
    var data = localStorage.getItem(key);
    if (!data) {
        localStorage.setItem(key, JSON.stringify([]));
    }
};

function saveLocalStorage(key, data) {
    var json = JSON.stringify(data);
    localStorage.setItem(key, json);
};

function addNewToLocalStorage(key, data) {
    var oldData = getLocalStorage(key);
    oldData.push(data);
    saveLocalStorage(key, oldData);
};

function editDataFromLocalStorage(key, data) {
    var oldData = getLocalStorage(key);
    var index = oldData.findIndex(function (infor) {
        return infor.account == data.account;
    });
    if (index == -1) return;
    oldData[index] = data;
    saveLocalStorage(key, oldData);
};

function removeOutOfLocalStorage(key, idStaff) {
    var oldData = getLocalStorage(key);
    // findIndex tìm phần tử trong object
    var index = oldData.findIndex(function (infor) {
        return infor.account == idStaff;
    });
    if (index == -1) {
        return;
    };
    // xóa phần tử khỏi danh sách
    oldData.splice(index, 1);
    saveLocalStorage(key, oldData);
};


// load localstorage
function getLocalStorage(key) {
    var dataJson = localStorage.getItem(key);
    // chuyển string sang array
    return JSON.parse(dataJson);
};

function openFormAddStaff() {
    resetForm();
    resetMsg();

};
// thêm nhân viên
function moreStaff() {
    var newStaff = getInformationFromForm();

    const valid = validateData(newStaff, true);
    console.log('valid: ', valid);
    if (!valid) return;

    addNewToLocalStorage(ST, newStaff)
    // lưu xuống danh sách sinh viên
    renderStafftList();

    resetForm();
};

// xóa sinh viên
function deleteStaff(idStaff) {
    removeOutOfLocalStorage(ST, idStaff)
    // sau khi xóa thì dữ liệu thay đổi , nhưng layout ko thay đổi vì layout đc tạo nhờ renderDSSVS
    renderStafftList();
};

// sửa nhân viên
function editStaff(idStaff) {
    resetMsg();
    const allData = getLocalStorage(ST);
    const staff = allData.find(function (staff) {
        return staff.account === idStaff
    })
    showInformationFromForm(staff)
};

// cập nhật nhân viên
function updateStaff() {
    var staffEdit = getInformationFromForm();

    const valid = validateData(staffEdit);
    if (!valid) return;

    editDataFromLocalStorage(ST, staffEdit)
    renderStafftList();
    resetForm();
};


function calcSalary(level, baseSalary) {
    const lv = levels.find(function (lv) {
        return lv.id === level
    })
    return lv ? lv.salary_rate * baseSalary : 0
    // if (selectValue.options[1].text == this.position) {
    //     var total = `${new Intl.NumberFormat("de-DE").format(
    //         this.baseSalary * 3
    //     )} VND`;
    // } else if (selectValue.options[2].text == this.position) {
    //     var total = `${new Intl.NumberFormat("de-DE").format(
    //         this.baseSalary * 2
    //     )} VND`;
    // } else if (selectValue.options[3].text == this.position) {
    //     var total = `${new Intl.NumberFormat("de-DE").format(
    //         this.baseSalary
    //     )} VND`;
    // }
    // return total;
};

function init() {
    var listLevelHtml = '<option value="">Chức vụ</option>'
    levels.forEach(function (lv) {
        listLevelHtml += `<option value="${lv.id}">${lv.name}</option>`
    })
    document.getElementById("chucvu").innerHTML = listLevelHtml;
    initLocalStorage(ST);
    renderStafftList();
};

function numberFormat(number) {
    return `${new Intl.NumberFormat("de-DE").format(number)} VND`;
};

function validateData(staff, isNew) {
    const validAcc = staff.validateAccount(isNew);
    const validFullName = staff.validateFullName();
    const validEmail = staff.validateEmail();
    const validPassword = staff.validatePassword();
    const validWorkDay = staff.validateWorkDay();
    const validBaseSalary = staff.validateBaseSalary();
    const validPosition = staff.validatePosition();
    const validWorkTime = staff.validateWorkTime();
    return validAcc && validFullName && validEmail && validPassword && validWorkDay && validBaseSalary && validPosition && validWorkTime;
};

// function validateDataAccount(staff) {
//     const validAcc = staff.validateAccount();
//     return validAcc;
// };


