function staffs(_account, _fullName, _email, _password, _workDay, _baseSalary, _position, _workTime, _totalSalary) {
    this.account = _account;
    this.fullName = _fullName;
    this.email = _email;
    this.password = _password;
    this.workDay = _workDay;
    this.baseSalary = _baseSalary * 1;
    this.position = _position;
    this.workTime = _workTime * 1;
    this.totalSalary = _totalSalary;
    // method trả về tổng lương nhân viên
    // this.totalEmployeeSalary = function () {
    //     var levelId = document.getElementById("chucvu").value;
    //     console.log('levels: ', levels);
    //     console.log('levelId: ', levelId);
    // if (selectValue.options[1].text == this.position) {
    //     var total = `${new Intl.NumberFormat("de-DE").format(
    //         this.baseSalary * 3
    //     )} VND`;
    // } else if (selectValue.options[2].text == this.position) {
    //     var total = `${new Intl.NumberFormat("de-DE").format(
    //         this.baseSalary * 2
    //     )} VND`;
    // } else if (selectValue.options[3].text == this.position) {
    //     var total = `${new Intl.NumberFormat("de-DE").format(
    //         this.baseSalary
    //     )} VND`;
    // }
    // return total;
    // };

    this.getClassification = function () {
        for (const cond of classificationCond) {
            if (this.workTime >= cond.time) {
                return cond.name
            };
        };
    };

    this.getPosition = function () {
        const lv = levels.find((lv) => {
            return lv.id === this.position
        });
        return lv ? lv.name : '';
    };

    this.validateAccount = function (isNew = false) {
        var errorMsg = '';
        if (this.account) {
            // check trung
            // 1. Lấy ra toan bo data từ ls
            const allData = getLocalStorage(ST);
            // 2. Kiem tra trung
            var _this = this
            const checkAccountDuplicate = isNew ? allData.find(function (item) {
                return item.account === _this.account;
            }) : null;
            if (checkAccountDuplicate) {
                errorMsg = "Tài khoản đã tồn tại";
            } else {
                // check hop le dua vao regex
                const pattern = /^[0-9]{4,6}$/;
                var isAccount = pattern.test(this.account);
                if (!isAccount) {
                    errorMsg = "Tài khoản tối đa 4 - 6 ký số";
                };
            };
        } else {
            errorMsg = 'không để trống';
        };
        // show msg
        document.getElementById('tbTKNV').innerText = errorMsg;
        return errorMsg == '';
    };
    this.validateFullName = function () {
        var errorMsg = '';
        if (this.fullName) {
            // check hop le dua vao regex
            const pattern = /^\D{0,}$/;
            var isFullName = pattern.test(this.fullName);
            if (!isFullName) {
                errorMsg = "Tên nhân viên phải là chữ";
            };
        } else {
            errorMsg = 'không để trống';
        };
        // show msg
        document.getElementById('tbTen').innerText = errorMsg;
        return errorMsg == ''
    };
    this.validateEmail = function () {
        var errorMsg = ''
        if (this.email) {
            // check hop le dua vao regex
            const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var isEmail = pattern.test(this.email);
            if (!isEmail) {
                errorMsg = "Email phải đúng định dạng. VD: hoang@gmail.com";
            };
        } else {
            errorMsg = 'không để trống';
        };
        // show msg
        document.getElementById('tbEmail').innerText = errorMsg;
        return errorMsg == '';
    };
    this.validatePassword = function () {
        var errorMsg = '';
        if (this.password) {
            // check hop le dua vao regex
            const pattern = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/;
            var isPassword = pattern.test(this.password);
            if (!isPassword) {
                errorMsg = "Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
            };
        } else {
            errorMsg = 'không để trống';
        };
        // show msg
        document.getElementById('tbMatKhau').innerText = errorMsg;
        return errorMsg == '';
    };
    this.validateWorkDay = function () {
        var errorMsg = '';
        if (this.workDay) {
            // check hop le dua vao regex
            const pattern = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/?|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:(?:0?2)(\/?|-|\.)(?:29)\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/?|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
            var isWorkDay = pattern.test(this.workDay);
            if (!isWorkDay) {
                errorMsg = "Định dạng mm/dd/yyyy";
            };
        } else {
            errorMsg = 'không để trống';
        };
        // show msg
        document.getElementById('tbNgay').innerText = errorMsg;
        return errorMsg == '';
    };
    this.validateBaseSalary = function () {
        var errorMsg = '';
        if (this.baseSalary) {
            if (this.baseSalary < 1e6 || this.baseSalary > 2e7) {
                errorMsg = "Lương cơ bản 1 000 000 - 20 000 000"
            };
        } else {
            errorMsg = 'không để trống';
        };
        // show msg
        document.getElementById('tbLuongCB').innerText = errorMsg;
        return errorMsg == ''
    };
    this.validatePosition = function () {
        var errorMsg = '';

        if (!this.position) {
            errorMsg = 'không để trống'
        };
        // show msg
        document.getElementById('tbChucVu').innerText = errorMsg;
        return errorMsg == '';
    };
    this.validateWorkTime = function () {
        var errorMsg = '';

        if (this.workTime) {
            if (this.workTime < 80 || this.workTime > 200) {
                errorMsg = "số giờ làm 80h - 200h";
            };
        } else {
            errorMsg = 'không để trống';
        };

        // show msg
        document.getElementById('tbGiolam').innerText = errorMsg;
        return errorMsg == ''
    };
};